def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, repeat = 2)
  ([word] * repeat).join(' ')
end

def start_of_word(word, letters)
  word.slice(0, letters)
end

def first_word(phrase)
  phrase.split.first
end

def titleize(words)
  little_words = %(and over the)

  words_arr = words.split.each_with_index.map do |word, i|
    little_words.include?(word) && i != 0 ? word : word.capitalize
  end

  words_arr.join(' ')
end
