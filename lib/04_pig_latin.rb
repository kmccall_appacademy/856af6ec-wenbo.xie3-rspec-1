def translate(str)
  vowels = %w(a e i o u)
  punctuation = ",.!?;:'"
  latinized_word = ""

  latinized_arr = str.split.map do |word|
    i = 0
    while i < word.length
      if vowels.include?(word[i])
        if word[i - 1] == "q"
          if word[0] == word[0].upcase
            latinized_word = word[i+1..-1] + word[0..i] + "ay"
            latinized_word = latinized_word.downcase.capitalize
          else
            latinized_word = word[i+1..-1] + word[0..i] + "ay"
          end
        else
          if word[0] == word[0].upcase
            latinized_word = word[i..-1] + word[0...i] + "ay"
            latinized_word = latinized_word.downcase.capitalize
          else
            latinized_word = word[i..-1] + word[0...i] + "ay"
          end
        end

        break
      end

      i += 1
    end

    latinized_word
  end

  latinized_arr.join(" ")
end
