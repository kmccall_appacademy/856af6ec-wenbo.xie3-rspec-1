def ftoc(temp)
  (temp - 32) * 5.fdiv(9)
end

def ctof(temp)
  temp * 9.fdiv(5) + 32
end
